package password;

public class PasswordValidator {
	private static int Min_length=8;
	private static char a;
    
	public static boolean isValidLength(String password )
	{
		
		return password.indexOf( " " )<0 && password.length()>=Min_length;
		
	}
	public static boolean isValidDigit(String password )
	{
		int digit = 0;

			 for(int index = 0; index < password.length(); index++ ){   
		        a = password.charAt( index );    
		        if( Character.isDigit(a) ){
		            digit++;
		        } 
		    }  
		    if( digit >= 2 && password.matches("[a-zA-Z0-9]+"))
		    {
		        return true;
		    }
		   
		    else 
		    {
		    	return false;
		    }
		
	}
}
