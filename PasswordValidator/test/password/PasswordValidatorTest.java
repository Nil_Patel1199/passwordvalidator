package password;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import org.junit.Test;

public class PasswordValidatorTest {
	
	@Test
	public void TestIsValidDigit( ) {
		boolean result = PasswordValidator.isValidDigit("42");
		assertTrue("Test failed",result);
	}
	@Test
	public void TestIsValidDigitException( ) {
		boolean result = PasswordValidator.isValidDigit(" ");
		assertFalse("Test failed",result);
	}	
	@Test
	public void TestIsValidDigitBoundaryIn( ) {
		boolean result = PasswordValidator.isValidDigit("22");
		assertTrue("Test failed",result);
	}	
	@Test
	public void TestIsValidLength( ) {
		boolean result = PasswordValidator.isValidLength("1234567890");
		assertTrue("Test failed",result);
	}
	@Test
	public void TestIsValidLengthException( ) {
		boolean result = PasswordValidator.isValidLength("1");
		assertFalse("Test failed",result);
	}
	@Test
	public void TestIsValidLengthBoundaryIn( ) {
		boolean result = PasswordValidator.isValidLength("123456789");
		assertTrue("Test failed",result);
	}
	
	@Test
	public void TestIsValidLengthBoundaryOut( ) {
		boolean result = PasswordValidator.isValidLength("13233  242");
		assertFalse("Test failed",result);
	}
	

}
